import React, { useState, useEffect } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import axios from 'axios';
import { fetchPlacements,updateconfig } from './api';
import api from './api';



function House({ house,houseId, users, onMoveUser, onDeleteUser }) {
  return (
    <div className="house">
      <h2>House {house}</h2>
      <div className="users">
        {users.map((user) => (
          <div key={user.id} className="user">
            <span>{user.user}</span>
            <button onClick={() => onMoveUser(user.id, houseId - 1)}>Up</button>
            <button onClick={() => onMoveUser(user.id, houseId + 1)}>Down</button>
            <button onClick={() => onDeleteUser(user.id)}>Delete</button>
          </div>
        ))}
      </div>
    </div>
  );
}

function HousingPage() {

  const [housingPlacements, setHousingPlacements] = useState([]);
  const [houseorder, setHouseOrder] = useState([]);
  const [newUser, setNewUser] = useState("");
  const [newUserHouse, setNewUserHouse] = useState("");
  const [newHouse, setNewHouse] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const housings = await fetchPlacements();
      setHousingPlacements(housings);
    //  console.log(housings[0].house);
    var order=[];
    var dict={};
    for (var i = 0; i < housings.length; i++) {
      dict[housings[i].house]=1;
      //setHouseOrder([...houseorder, {"houseId":i,"house":housings[i].house } ]);
    }
    for (var i = 0; i < Object.keys(dict).length; i++){
      order.push({"houseId":i,"house":Object.keys(dict)[i] });
    }
    setHouseOrder(order);
    };
    fetchData();
  }, []);

  function handleMoveUser(userId, houseIndex) {
    // update data on backend API and local state
    console.log(housingPlacements);
    console.log(houseorder);
    console.log(houseIndex);

    if (houseIndex==-1) houseIndex=houseorder.length-1;
    if (houseIndex==houseorder.length) houseIndex=0;
    //console.log(houseIndex);
    const updatedData = housingPlacements.map((item) => {
      if (item.id === userId) {
        const updateuserhouse = {
          "id": userId,
          "user": item.user,
          "house": houseorder[houseIndex].house
        };
        api.put('/saveconfig', updateuserhouse, {
          headers: {
            'Content-Type': 'application/json'
          }
        }).catch(error => console.error(error));

        return {
          ...item,
          house: houseorder[houseIndex].house ,

        };
      }
      return item;
    });

    setHousingPlacements(updatedData);
    console.log(updatedData);
    //const houses = await updateconfig(formData);
  }

  function handleDeleteUser(userId) {
    // delete user from backend API and local state
    const updatedData = housingPlacements.filter((item) => item.user.id !== userId);
    setHousingPlacements(updatedData);
    api.delete(`/deleteconfig/${userId}`)
      .catch(error => console.error(error));
    window.location.reload();
  }



  function handleAddUser() {
    if (newUser === "" || newUserHouse === "") {
    alert("Please fill out both fields user and house");
    return;}

    const newUserObject = {
      "id": -1,
      "user": newUser,
      "house": newUserHouse
    };
    api.put('/saveconfig', newUserObject, {
      headers: {
        'Content-Type': 'application/json'
      }
    }).catch(error => console.error(error));
    setNewUser("");
    setNewUserHouse("");
    window.location.reload();

  }

  function handleAddHouse() {
  }

  function handleSubmit() {
  }

  //const housesorder=
  const houses = Array.from(new Set(houseorder.map((item) => item.house)));
  return (
    <div>
    <h1>Housing config</h1>
    <div className="housingconfig">
      {houses.map((house,houseId) => (
        <House
          key={house}
          house={house}
          houseId={houseorder[houseId].houseId}
          users={housingPlacements.filter((item) => item.house === house)}
          onMoveUser={handleMoveUser}
          onDeleteUser={handleDeleteUser}
        />
      ))}
    </div>
    <br></br>
    <br></br>
    <div className="add">
    <input type="text" value={newUser} onChange={(e) => setNewUser(e.target.value)} placeholder="Enter username" />
    <input type="number" value={newUserHouse} onChange={(e) => setNewUserHouse(e.target.value)} placeholder="Enter house" />
    <button onClick={handleAddUser}>Add User</button>
    </div>

    </div>
  );
}

export default HousingPage;
