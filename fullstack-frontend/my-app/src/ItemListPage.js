import React from 'react';

function ItemListPage({ items }) {
  return (
    <div>
      <h1>Item List Page</h1>
      <table>
        <thead>
          <tr>
            <th>Item</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>
          {items.map( (item) => (
            <tr key={item.id}>
              <td>{item.name}</td>
              <td>{item.quantity}</td>
            </tr>
          ) ) }
        </tbody>
      </table>
    </div>
  );
}

export default ItemListPage;
