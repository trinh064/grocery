import React, { useState, useEffect } from "react";
import { fetchItems,submitOrder } from './api';
import { Button, ListGroup } from 'react-bootstrap';
import ImageDisplay from './ImageDisplay';

function ShoppingPage(props) {
  const [items, setItems] = useState([]);
  const [order, setOrder] = useState([]);

  useEffect(() => {
    const fetchData= async () => {
      const items = await fetchItems();
      setItems(items);
    };
    fetchData();
  }, []);

  const handleAddToOrder = (item) => {
    console.log(order);
    if (order.includes(item) ){
      item.quantity+=1;
      setOrder([...order]);
    }
    else{
      item.quantity=1;
      setOrder([...order, item]);
    }
  };

  const handleSubmitOrder = async () => {
    //console.log(order.length);
    var orders=[];
    for(var i=0;i<order.length;i++){
      //console.log(i,order[i].quantity);
      for( var c=0;c<order[i].quantity;c++){
        orders.push ({"date":new Date(props.date).setHours(-1),"house":props.house,"item":order[i].name,"user":props.user} );
      }
    }
    const suc=await submitOrder(orders);
    if(suc){ window.alert('Submited!');     window.location.reload();  } else { window.alert('Failed to submit');}


  };

  const [currentPage, setCurrentPage] = useState(0);

  const itemsPerPage = 8;
  const totalPages = Math.ceil(items.length / itemsPerPage);

  const startIndex = currentPage * itemsPerPage;
  const endIndex = Math.min(startIndex + itemsPerPage, items.length);

  const handleNextPage = () => {
    setCurrentPage((currentPage + 1) % totalPages);
  };

  const handlePrevPage = () => {
    setCurrentPage((currentPage - 1 + totalPages) % totalPages);
  };

  const getRows = () => {
    const rows = [];
    for (let i = startIndex; i < endIndex; i += 4) {
      rows.push(
        <div className="item-row" key={i}>
          <tr>
            {items.slice(i, i + 4 ).map((item) => (
              <td>
                <div
                  key={item.id}
                  className="item-card"
                  style={{
                    backgroundColor:
                      item.shopname === "Costco" ? "#ADD8E6" : "#FFC0CB",
                  }}
                >
                  <img
                    src={item.icon}
                    style={{ width: `250px`, height: `250px` }}
                    alt="image not found"
                  />
                  <p>
                    {item.name} {item.price}$/{item.unit}
                  </p>
                  <button onClick={() => handleAddToOrder(item)}>
                    Add to Order
                  </button>
                </div>
              </td>
            ))}
          </tr>
        </div>
      );
    }
    return rows;
  };


  return (
    <div>
      <h1>Available Items</h1>
      <div className="container-fluid d-flex justify-content-center align-items-center">
        <div className="container text-center">
          <div className="row justify-content-center">
            <div className="col-lg-8">
              <div className="items-container">
                {getRows()}
                <div className="pagination mt-3">
                  {currentPage > 0 && (
                    <button className="btn btn-secondary me-2" onClick={handlePrevPage}>Prev</button>
                  )}
                  <span>
                    {currentPage + 1}/{totalPages}
                  </span>
                  {currentPage < totalPages - 1 && (
                    <button className="btn btn-secondary ms-2" onClick={handleNextPage}>Next</button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <h1>Cart</h1>
      <div className="order-container mt-4">
        <div className="row">
          <div className="col-md-6">
            <h5 className="text-center text-primary">Costco Items</h5>
            <table className="table table-striped">
              <tbody>
                {order
                  .filter((item) => item.shopname === "Costco")
                  .map((item) => (
                    <tr key={item.id}>
                      <td>{item.name}</td>
                      <td>x {item.quantity}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
          <div className="col-md-6">
            <h5 className="text-center text-danger">Spicehut Items</h5>
            <table className="table table-striped">
              <tbody>
                {order
                  .filter((item) => item.shopname !== "Costco")
                  .map((item) => (
                    <tr key={item.id}>
                      <td>{item.name}</td>
                      <td>x {item.quantity}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <button onClick={handleSubmitOrder}>Submit Order</button>
    </div>
  );
}

export default ShoppingPage;
