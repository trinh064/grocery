import React, { useState, useEffect } from 'react';

function ImageDisplay(props) {
  const [imageSrc, setImageSrc] = useState('');

  useEffect(() => {
    if (props.byte) {
      const base64String = btoa(
        new Uint8Array(props.byte).reduce(
          (data, byte) => data + String.fromCharCode(byte),
          ''
        )
      );
      setImageSrc(`data:${props.name}/png;base64,${base64String}`);
    }
  }, [props.byte]);

  return (
    <div>
      {imageSrc && <img src="https://cdn-icons-png.flaticon.com/512/4436/4436481.png" style={{ width: `10px`, height: `10px` }} alt="image not found" />}
    </div>
  );
}

export default ImageDisplay;
