
import React, { useState, useEffect } from 'react';
import api from './api';

function ItemsByWeek() {
  const [date, setDate] = useState('');
  const [weeks, setWeeks] = useState([]);

  useEffect(() => {
    // Fetch the list of previous 12 weeks when the component mounts
    fetchWeeks();
  }, []);

  const fetchWeeks = async () => {
    try {
      const response = await api.get('/weeks');
      setWeeks(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await api.get(`/items?week=${date}`);
      // handle the response and navigate to the list of items page
    } catch (error) {
      console.error(error);
    }
  };

  const handleWeekClick = async (week) => {
    try {
      const response = await api.get(`/week/items?date=${week}`);
      // handle the response and navigate to the list of items page
      console.log(response);

    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1>Date Selection Page</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Select a date:
          <input type="date" value={date} onChange={(event) => setDate(event.target.value)} />
        </label>
        <button type="submit">Submit</button>
      </form>
      <h2>Previous 12 weeks:</h2>
      <ul>
        {weeks.map((week) => (
          <li key={week}>
            <button onClick={() => handleWeekClick(week)}>{week}</button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ItemsByWeek;
