import React, { useState } from "react";

function ItemList({ items, handleAddToOrder }) {
  const [currentPage, setCurrentPage] = useState(0);

  const itemsPerPage = 9;
  const totalPages = Math.ceil(items.length / itemsPerPage);

  const startIndex = currentPage * itemsPerPage;
  const endIndex = Math.min(startIndex + itemsPerPage, items.length);

  const handleNextPage = () => {
    setCurrentPage((currentPage + 1) % totalPages);
  };

  const handlePrevPage = () => {
    setCurrentPage((currentPage - 1 + totalPages) % totalPages);
  };

  return (
    <div className="items-container">
      {items.slice(startIndex, endIndex).map((item) => (
        <div key={item.id} className="item-card">
          <img src={item.icon} alt={item.name} />
          <h2>
            {item.name} {item.price}$/{item.unit}
          </h2>
          <button onClick={() => handleAddToOrder(item)}>Add to Order</button>
        </div>
      ))}
      <div className="pagination">
        {currentPage > 0 && (
          <button onClick={handlePrevPage}>Prev</button>
        )}
        <span>
          {currentPage + 1}/{totalPages}
        </span>
        {currentPage < totalPages - 1 && (
          <button onClick={handleNextPage}>Next</button>
        )}
      </div>
    </div>
  );
}

export default ItemList;
