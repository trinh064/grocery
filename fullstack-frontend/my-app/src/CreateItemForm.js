import React, { useState } from 'react';
import { submitItem } from './api';

const CreateItemForm = () => {
  const [name, setName] = useState('');
  const [icon, setIcon] = useState("");
  const [unit, setUnit] = useState('');
  const [price, setPrice] = useState('');
  const [shopname, setShopname] = useState('');


  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('id',-1 );
    formData.append('name', name);
    formData.append('icon', icon);
    formData.append('unit', unit);
    formData.append('price', price);
    formData.append('shopname', shopname);

    // Send the form data to the backend here
    const houses = await submitItem(formData);

    console.log(formData);
    window.alert('Item added!');
    window.location.href = '/edititem';

  };



  return (
    <form onSubmit={handleSubmit}>
      <label>
        Name:
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </label>
      <br />
      <label>
        Icon url:
        <input type="url" value={icon} onChange={(e) => setIcon(e.target.value)} />
      </label>
      <br />
      <label>
        Unit:
        <input type="text" value={unit} onChange={(e) => setUnit(e.target.value)} />
      </label>
      <br />
      <label>
        Price:
        <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
      </label>
      <br />
      <label>
        Shop Name:
        <input type="text" value={shopname} onChange={(e) => setShopname(e.target.value)} />
      </label>
      <br />
      <button type="submit">Submit</button>
    </form>
  );
};

export default CreateItemForm;
