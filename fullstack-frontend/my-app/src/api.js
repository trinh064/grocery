import axios from 'axios';

const API_BASE_URL = 'http://ec2-18-210-10-12.compute-1.amazonaws.com/api';

const instance = axios.create({
  baseURL: API_BASE_URL,
});
export const fetchDates = async () => {
  const response = await axios.get(`${API_BASE_URL}/dates`);
  return response.data;
};

export const fetchHousesForDate = async (date) => {
  //var response = await axios.get(`${API_BASE_URL}/housesbyweek?date=${date}`);
  //var data=response.data;
  //if (data.length==0) {
  const  response=await axios.get(`${API_BASE_URL}/getconfig`);
  var   data=response.data
     var dict={};
     for (var i = 0; i < data.length; i++) {
       dict[ data[i]["house"] ]=1;
    }
    data=Object.keys(dict);

  return data;
};

export const fetchUsersForHouse = async (house,date) => {
  //var response = await axios.get(`${API_BASE_URL}/usersbyweek?house=${house}&date=${date}`);
  //var data=response.data;
  const  response=await axios.get(`${API_BASE_URL}/getconfig`);
  var   data=response.data
     var dict={};
     for (var i = 0; i < data.length; i++) {
       if(data[i]["house"]===house)
       dict[ data[i]["user"] ]=1;
    }
    data=Object.keys(dict);
  return data;
};

export const updateconfig =  (config) => {
  //var response = await axios.get(`${API_BASE_URL}/usersbyweek?house=${house}&date=${date}`);
  //var data=response.data;
  var success=1;
  axios.put(`${API_BASE_URL}/saveconfig`,config).catch((error) => {
      success=0;
    });

return success;
};

export const fetchItems = async () => {
  const response = await axios.get(`${API_BASE_URL}/items`);
  return response.data;
};

export const fetchOrdersForUser = async (user,date) => {
  const response = await axios.get(`${API_BASE_URL}/items/user?user=${user}&date=${date}`);
  return response.data;
};

export const fetchPlacements = async () => {
  const response = await axios.get(`${API_BASE_URL}/getconfig`);
  return response.data;
};

export const submitOrder = async (order) => {
  var success=1;
   for (var i = 0; i < order.length; i++) {
   // console.log(order[i]["date"]);
   // order[i]["date"].setHours(order[i]["date"] + 24); 
   // console.log(order[i]["date"]);

    var rec= order[i];
    const response = await axios.put(`${API_BASE_URL}/add`, rec).catch((error) => {
        success=0;
      });
  }
  return success;
};

export const submitItem = async (FormData) => {
  const response = await axios.put(`${API_BASE_URL}/edititem`,FormData);
  return response.data;
};

export const delDate = async (date)=> {
  console.log(date);
  
  if (window.confirm('Are you sure you want to delete this data?')){
    const response = await  axios.delete(`${API_BASE_URL}/deletedate?date=${date}`);
  }
  window.location.reload();
  //return response.data;
}
export default instance;
