import React, { useState, useEffect } from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import { fetchDates, fetchHousesForDate, fetchUsersForHouse, fetchOrdersForUser,submitOrder,delDate } from './api';
import Navbar from "./Navbar";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ShoppingPage from "./ShoppingPage"
import { Table } from "react-bootstrap";
import moment from 'moment';

function Home() {
  const [currentComponent, setCurrentComponent] = useState('component1');
  const [dates, setDates] = useState([]);
  const [date, setDate] = useState([]);

  const [houses, setHouses] = useState([]);
  const [house, setHouse] = useState([]);

  const [users, setUsers] = useState([]);
  const [user, setUser] = useState([]);

  const [orders, setOrders] = useState([]);
  const [order, setOrder] = useState([]);

  const [selectedDate, setSelectedDate] = useState(null);

  const handleDateChange = (event) => {
    const dateString = event.target.value; // get the date string from the input field
    //const date = new Date(dateString).setHours(0, 0, 0, 0); // convert the date string to a Date object
    //const formattedDate = moment(date).format('YYYY-MM-DD'); // format the date using the toLocaleDateString method
    setSelectedDate(dateString);
  };

  const handleSubmitDate = async () => {
    console.log(order.length);
    var orders=[];
  //  const date = new Date(); // get the current date and time
//date.setHours(date.getHours() + 24); // add 24 hours to the date
//console.log(date); // display the new date value
    orders.push ({"date":new Date(selectedDate).setHours(23),"house":null,"item":null,"user":null} );
    const suc=await submitOrder(orders);
    if(suc){ window.alert('Date added!');} else { window.alert('Failed to add date');}
    window.location.reload();
  };

  const handleAddDate = () => {
    if (selectedDate !== null) {
      //setDates([...dates, selectedDate]);
      setSelectedDate(null);
      handleSubmitDate();

    }
  };

  const handleDeleteDate = (date) => {
    //if (window.confirm('Are you sure you want to delete this data?'))
    //{delDate(date);}
    delDate(date);
    //window.location.reload();
  };

  useEffect(() => {
    const fetchData = async () => {
      const dates = await fetchDates();

    //  for (var i = 0; i < dates.length; i++) {
    //    console.log(new Date(dates[i]));
     //   dates[i]=new Date(dates[i]).toISOString();
    // }
      setDates(dates);

    };
    fetchData();
  }, []);

  const handleDateClick = async (date) => {
    const houses = await fetchHousesForDate(date);
    setHouses(houses);
    setDate(date);
    setUsers([]);
    setOrders([]);
  };

  const handleHouseClick = async (house) => {
    const users = await fetchUsersForHouse(house ,date);
    setHouse(house);
    setUsers(users);
    setOrders([]);
  };

  const handleUserClick = async (user,date) => {
    const orders = await fetchOrdersForUser(user,date);
    //setOrder(order);
    setUser(user);
    setOrders(orders);
  };


  function renderCurrentComponent() {
    switch (currentComponent) {
      case 'component1':
        return (
            <div className="col-4">
            <div>
              <input type="date" value={selectedDate} onChange={handleDateChange} />
              <button onClick={handleAddDate}>Add</button>
              {dates.map((date) => (
                <button key={date} disabled>
                  {date}
                </button>
              ))}
            </div>
              <h2>Dates</h2>
              <ListGroup>
                {dates.map((date) => (
                  <div>
                  <Button key={date} variant="light" className="mb-2" onClick={() => {handleDateClick(date);setCurrentComponent('component2')} }>
                    {date}
                  </Button>
                  <Button variant="light" className="mb-2 ml-2" onClick={() => handleDeleteDate(date)}>X</Button> 
                  </div>
                ))}
                </ListGroup>
          </div>
        );

      case 'component2':
        return (
          <div>
            <button onClick={() => setCurrentComponent('component1')}>Back</button>
            <div className="col-4">
              <h2>Week: {date}  </h2>
              <h2>Houses</h2>
              <ListGroup>
                {houses.map((house) => (
                  <Button key={house} variant="light" className="mb-2" onClick={() => {handleHouseClick(house,date);setCurrentComponent('component3');} }>
                    {house}
                  </Button>
                ))}
              </ListGroup>
            </div>
          </div>
        );
      case 'component3':
        return (
          <div>
          <button onClick={() => setCurrentComponent('component2')}>Back</button>
          <div className="col-4">
            <h2>Week: {date}  </h2>
            <h2>Houses: {house}</h2>
            <h2>Users</h2>
            <ListGroup>
              {users.map((user) => (
                <Button key={user} variant="light" className="mb-2" onClick={() => {handleUserClick(user,date);setCurrentComponent('component4');}  }>
                  {user}
                </Button>
              ))}
            </ListGroup>
          </div>
          </div>
        )  ;

        case 'component4':
          return (
            <div>
            <button onClick={() => setCurrentComponent('component3')}>Back</button>
            <div className="mt-5">
            <h2>Week: {date}  </h2>
            <h2>Houses: {house}</h2>
            <h2>Users: {user}</h2>
            <h2>Orders</h2>
            <Table >
              <thead>
                <tr>
                  <th>Item</th>
                  <th>Quantity</th>
                </tr>
              </thead>
              <tbody>
                {orders.map((order, index) => (
                  <tr key={index} style={{color: order[2] === "Costco" ? "blue" : "red"}}>
                    <td>{order[0]}</td>
                    <td>{order[1]}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <ShoppingPage date={date} house={house} user= {user}/>
            </div>

            </div>
          )  ;



      default:
        return null;
    }
  }

  return (

    <div>  {renderCurrentComponent()}</div>
  );
}

export default Home;
