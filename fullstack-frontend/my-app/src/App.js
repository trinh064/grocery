import "./App.css";
import Navbar from "./Navbar";
import Home from "./Home";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ShoppingPage from "./ShoppingPage"
import CreateItemForm from "./CreateItemForm"
import Export from "./Export"
import HousingPage from "./HousingPage";
import React from 'react';

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />

        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/housing" element={<HousingPage />} />
          <Route exact path="/shopping" element={<ShoppingPage />} />
          <Route exact path="/export" element={<Export />} />
          <Route exact path="/edititem" element={<CreateItemForm />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
