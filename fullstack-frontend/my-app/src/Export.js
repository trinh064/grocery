import React, { useState, useEffect } from 'react';
import axios from "axios";
import { CSVLink } from "react-csv";
import { Button, ListGroup } from 'react-bootstrap';
import { fetchDates } from './api';


function Export() {
  const [availableDates, setAvailableDates] = useState([]);
  const [selectedDate, setSelectedDate] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const dates = await fetchDates();
      setAvailableDates(dates);
    };
    fetchData();
  }, []);

  const handleDownloadCsv = () => {
    // Fetch the CSV data for the selected date from the backend
    fetch(`http://ec2-18-210-10-12.compute-1.amazonaws.com/api/week/items?date=${selectedDate}`)
      .then(response => response.json())
      .then(data => {
        // Generate the CSV content as a string
        let csvContent = 'shopname,item,quantity\n';
        data.forEach(([item, quantity,shopname]) => {
          if (shopname != null ) csvContent += `${shopname},${item},${quantity}\n`;
        });

        // Create a new Blob object with the CSV content
        const blob = new Blob([csvContent], { type: 'text/csv' });

        // Create a URL for the Blob object
        const url = URL.createObjectURL(blob);

        // Create a new link element
        const link = document.createElement('a');

        // Set the link's href attribute to the URL
        link.href = url;

        // Set the link's download attribute to the desired filename
        link.download = `${selectedDate}.csv`;

        // Simulate a click on the link element to download the file
        link.click();
      })
      .catch(error => console.error(error));
  };

  return (
    <div>
      <h1>Available Dates</h1>
      {availableDates.map(date => (
        <button key={date} onClick={() => setSelectedDate(date)}>{date}</button>
      ))}
      {selectedDate && (
        <div>
          <h2>Download CSV for {selectedDate}</h2>
          <button onClick={handleDownloadCsv}>Download CSV</button>
        </div>
      )}
    </div>
  );
}

export default Export;
