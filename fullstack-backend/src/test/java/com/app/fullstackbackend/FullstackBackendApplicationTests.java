package com.app.fullstackbackend;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class FullstackBackendApplicationTests {

	@Test
    public void testAddition() {
       
        
        assertEquals(5, 5);
    }

    @Test
    public void testSubtraction() {
       
        assertEquals(2, 2);
    }

}
