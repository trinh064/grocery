package com.app.fullstackbackend.model;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;




@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class RecordController {

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private PlacementRepository placementRepository;

    @Autowired
    private ItemRepository itemRepository;

    @GetMapping("/getconfig")
    public List<Placement> getconfig(){
        return placementRepository.findAll();
    }
    @PutMapping("/saveconfig")
    public Placement saveconfig(@RequestBody Placement p){
        //System.out.println.log(p.toString());
        return placementRepository.save(p);
    }
    @DeleteMapping("/deleteconfig/{id}")
    public void delconfig(@PathVariable Long id){
        //System.out.println.log(p.toString());
         placementRepository.deleteById(id);
         return;
    }
    
    @DeleteMapping("/deletedate")
    public void deldate(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date){
        System.out.println( date );
        recordRepository.deleteByDate(date);
         return;
    }

    @DeleteMapping("/deleteitem")
    public void delitem(@RequestParam("item") String item){
        recordRepository.deleteByItem(item);
         return;
    }


    /* @PutMapping("/saveconfig")
    public void saveconfig(@RequestBody List<Placement> newlistplacement){

        List<Placement> oldlistplacement= placementRepository.findAll();
        for ( Placement p : listplacement){
            p 
        }
        return ;
    }*/
    

    @GetMapping("/items")
    public List<Item> getItems() {
        return itemRepository.findAll();
    }

    
    @PutMapping("/edititem")
    public Item createItem(@RequestParam("name") String name, @RequestParam("unit") String unit, @RequestParam("price") double price,
    @RequestParam("shopname") String shopname, @RequestParam("icon") String iconurl) throws IOException {
       
        List<Long> ids= itemRepository.findIdsByName(name);
        Item item = new Item();
        if (ids.size() >0 ) item.setId((Long)ids.get(0) );
        item.setName(name);
        item.setUnit(unit);
        item.setPrice(price);
        item.setShopname(shopname);
        item.setIcon(iconurl);
        itemRepository.save(item);
        return item;
    }

    @GetMapping("/recorditems")
    public List<String> getUniqueItems() {
        return recordRepository.findUniqueItems();
    }

    @GetMapping("/dates")
    public List<String> getDates() {
        return recordRepository.findDates();
    }

    @GetMapping("/items/user")
    public List<Object[]> getItemsByUserAndWeek(@RequestParam("user") String userId, @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        System.out.println( date );
        return recordRepository.findItemsByUserAndWeek(userId, date);
    }

    @GetMapping("/week/items")
    public List<Object[]> getItemsAndQuantitiesByWeek(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        System.out.println(date);

        return recordRepository.findItemsAndQuantitiesByWeek(date);
    }

    @GetMapping("/week/house/items")
    public List<String> getItemsByWeekAndHouse(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date, @RequestParam("house") String house ) {
        return recordRepository.findItemsByWeekAndHouse(date, house);
    }

    @GetMapping("/houses")
    public List<String> getHouses() {
        return recordRepository.findUniqueHouses();
    }

    @GetMapping("/housesbyweek")
    public List<String> getHousesByWeek(@RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return recordRepository.findHousesByWeek(date);
    }

    @GetMapping("/users")
    public List<String> getUsersByHouse(@RequestParam("house") String user) {
        return recordRepository.findUsersByHouse(user);
    }

    @GetMapping("/usersbyweek")
    public List<String> getUsersByHouseandWeek(@RequestParam("house") String house, @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return recordRepository.findUsersByHouseandWeek(house,date);
    }
    @PutMapping("/add")
    public Record saveRecord(@RequestBody Record record) {
    return recordRepository.save(record);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteRecord(@PathVariable Long id) {
    Optional<Record> record = recordRepository.findById(id);
    if (record.isPresent()) {
        recordRepository.delete(record.get());
        return ResponseEntity.ok().build();
    } else {
        return ResponseEntity.notFound().build();
    }

    }

}


@Repository
interface RecordRepository extends JpaRepository<Record, Long> {

    @Query(value = "SELECT item FROM record WHERE YEARWEEK(date) = YEARWEEK(:date) AND house=:house", nativeQuery = true)
    List<String> findItemsByWeekAndHouse(@Param("date") Date date, @Param("house") String house);
    
    @Transactional
    @Modifying
    @Query(value = "DELETE FROM record WHERE item=:item", nativeQuery = true)
    void deleteByItem(@Param("item") String item);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM record WHERE YEARWEEK(date) = YEARWEEK(:date)", nativeQuery = true)
    void deleteByDate(@Param("date") Date date);

    @Query(value = " SELECT r.item, COUNT(r.item) AS quantity, i.shopname FROM record r LEFT JOIN item i ON r.item = i.name WHERE YEARWEEK(r.date) = YEARWEEK(:date) GROUP BY r.item,i.shopname ORDER BY i.shopname ASC"  
    , nativeQuery = true)
    List<Object[]> findItemsAndQuantitiesByWeek(@Param("date") Date date);

    @Query(value = "SELECT DISTINCT item FROM record", nativeQuery = true)
    List<String> findUniqueItems();

    @Query(value = "SELECT DISTINCT house FROM record", nativeQuery = true)
    List<String> findUniqueHouses();

    @Query(value = "SELECT DISTINCT date FROM record ORDER BY date DESC", nativeQuery = true)
    List<String> findDates();

    @Query(value = "SELECT r.item, COUNT(r.item) AS quantity, i.shopname FROM record r LEFT JOIN item i ON r.item = i.name WHERE YEARWEEK(r.date) = YEARWEEK(:date) AND r.user=:user GROUP BY r.item,i.shopname ORDER BY i.shopname ASC", nativeQuery = true)
    List<Object[]> findItemsByUserAndWeek(@Param("user") String user, @Param("date") Date date);

    @Query(value = "SELECT DISTINCT user FROM record WHERE house = :house AND YEARWEEK(date) = YEARWEEK(:date)", nativeQuery = true)
    List<String> findItemsByWeekandHouse( @Param("date") Date date ,@Param("house") String house );

    @Query(value = "SELECT DISTINCT user FROM record WHERE house = :house ", nativeQuery = true)
    List<String> findUsersByHouse( @Param("house") String house );

    @Query(value = "SELECT DISTINCT user FROM record WHERE house = :house AND YEARWEEK(date) = YEARWEEK(:date)", nativeQuery = true)
    List<String> findUsersByHouseandWeek( @Param("house") String house ,  @Param("date") Date date );

    @Query(value = "SELECT DISTINCT house FROM record WHERE YEARWEEK(date) = YEARWEEK(:date)", nativeQuery = true)
    List<String> findHousesByWeek( @Param("date") Date date );
    
}

@Repository
interface PlacementRepository extends JpaRepository<Placement, Long> {

}

@Repository
interface ItemRepository extends JpaRepository<Item, Long> {
    @Query("SELECT p.id FROM Item p WHERE p.name = ?1")
    List<Long> findIdsByName(String name);
}

@Entity
class Item {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private String name;
    @Lob
    private String icon;
    private String unit;
    private double price;
    private String shopname;

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", icon='" + getIcon() + "'" +
            ", unit='" + getUnit() + "'" +
            ", price='" + getPrice() + "'" +
            ", shopname='" + getShopname() + "'" +
            "}";
    }
    
    // Constructors
    public Item() {}

    public Item(String name, String icon, String unit, double price, String shopname) {
        this.name = name;
        this.icon = icon;
        this.unit = unit;
        this.price = price;
        this.shopname = shopname;
    }

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getIcon() {
        return icon;
    }
    
    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }
}

@Entity
class Placement {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String user;
    private String house;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getHouse() {
        return this.house;
    }

    public void setHouse(String house) {
        this.house = house;
    }


}

@Entity
class Record {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private Date date;
    
    private String house;

    private String user;
    
    private String item;

    

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHouse() {
        return this.house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getItem() {
        return this.item;
    }

    public void setItem(String item) {
        this.item = item;
    }
    
  
    public Record() {
    }

    public Record(Date date, String house, String user, String item) {
        this.date = date;
        this.house = house;
        this.user = user;
        this.item = item;
    }

    // getters and setters for all fields
}
